﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rLibrary.Controls
{
    public class SelectorControl : Control
    {
        List<String> options;

        public SelectorControl(String name, int width = 50, int height = 20)
            : base(name, width, height)
        {
            options = new List<string>();
        }

        public override object Value
        {
            get
            {
                return base.Value;
            }
            set
            {
                if (Options.Contains(value))
                    base.Value = value;
            }
        }

        public List<String> Options
        {
            get
            {
                return options;
            }
            set
            {
                options = value;
            }
        }

        public void SetOption(int id)
        {
            if (id > options.Count - 1)
                id = options.Count - 1;
            else if (id < 0)
                id = 0;

            Value = Options[id];
        }
    }
}
