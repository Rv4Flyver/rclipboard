﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rLibrary.Controls
{
    public abstract class Control
    {
        public virtual String Name { get; set; }
        public virtual int Width { get; set; }
        public virtual int Height { get; set; }
        public virtual Object Value { get; set; }

        public Control()
        {

        }

        public Control(String name, int width = 50, int height = 20)
            : this()
        {
            Name = name;
            Width = width;
            Height = height;
        }
    }
}
