﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rLibrary.Controls
{
    public class LabelControl : Control
    {
        public LabelControl(String name, int width = 50, int height = 20)
            : base(name, width, height)
        {

        }

        public override object Value
        {
            get
            {
                return base.Value;
            }
            set
            {
                base.Value = value;
            }
        }
    }
}
