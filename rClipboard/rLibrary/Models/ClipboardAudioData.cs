﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rLibrary.Models
{
    public class ClipboardAudioData : ClipboardData
    {
        int size;
        DateTime updateTime;

        public override object Data
        {
            get;
            set;
        }

        public override string Description
        {
            get;
            set;
        }

        public override int Size
        {
            get
            {
                return size;
            }

        }

        public override DateTime AdditionTime
        {
            get
            {
                return updateTime;
            }

        }

        public ClipboardAudioData(object data, string description)
        {
            Data = data;
            Description = description;
            updateTime = DateTime.UtcNow;
        }
    }
}
