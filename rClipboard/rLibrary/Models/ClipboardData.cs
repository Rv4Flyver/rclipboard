﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;

namespace rLibrary.Models
{

    public abstract class ClipboardData
    {
        #region Properties
        public abstract Object Data
        {
            get;
            set;
        }

        public abstract string Description
        {
            get;
            set;
        }
        public abstract int Size
        {
            get;
        }
        public abstract DateTime AdditionTime
        {
            get;
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return Description + ";" + Size + ";" + AdditionTime;
        }
        #endregion
    }
}
