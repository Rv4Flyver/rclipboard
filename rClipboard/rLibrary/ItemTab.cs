﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using rLibrary.Models;

namespace rLibrary
{
    public class ItemTab : ITab
    {
        Func<ClipboardData, ClipboardData> execDelegate;

        #region Properties
        public virtual string Label
        {
            get;
            set;
        }

        public virtual ContextType TabType
        {
            get;
            private set;
        }

        public virtual string Base64Image
        {
            get;
            set;
        }

        public virtual Func<ClipboardData, ClipboardData> ExecutionDelegate
        {
            set
            {
                if (execDelegate == null)
                {
                    execDelegate = value;
                }
            }
        }
        #endregion

        #region Ctors
        private ItemTab()
        {

        }

        public ItemTab(string label, ContextType tabType, Func<ClipboardData, ClipboardData> execuctionDelegate, string base64Image = "")
            : this()
        {
            Label = label;
            Base64Image = base64Image;
            execDelegate = execuctionDelegate;
            TabType = tabType;
        }
        #endregion

        #region Methods
        public virtual ClipboardData Execute(ClipboardData data)
        {
            return execDelegate(data);
        }
        #endregion
    }
}
