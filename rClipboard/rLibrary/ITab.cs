﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace rLibrary
{
    public enum ContextType { Audio, Image, Text, Undefined }

    public interface ITab
    {

        #region Properties
        string Label
        {
            get;
            set;
        }

        ContextType TabType
        {
            get;
        }

        string Base64Image
        {
            get;
            set;
        }
        #endregion
    }
}
