﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace rLibrary
{
    public class CategoryTab : ITab
    {
        #region Properties
        public virtual string Label
        {
            get;
            set;
        }

        public virtual string Base64Image
        {
            get;
            set;
        }

        public virtual ContextType TabType
        {
            get;
            private set;
        }

        public virtual List<ITab> Tabs
        {
            get;
            set;
        }

        public virtual List<Controls.Control> Controls
        {
            get;
            private set;
        }
        #endregion

        #region Ctors
        public CategoryTab()
        {
            Tabs = new List<ITab>();
        }

        public CategoryTab(string label, ContextType tabType, List<Controls.Control> controls = null, string base64Image = "")
            : this()
        {
            Label = label;
            Base64Image = base64Image;
            TabType = tabType;
            Controls = controls;
        }
        #endregion

        public virtual List<ITab> Expand()
        {
            return Tabs;
        }
    }
}
