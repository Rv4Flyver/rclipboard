﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using rLibrary;
using rLibrary.Models;
using rLibrary.Controls;

namespace rPlugin
{
    static public class Plugin
    {
        static string ccl_base64Image = "iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAA/vSURBVHja3FrJb2THff5+tbylV/L1wuEyHGooDkdSDIxiHWzEWw4OcjOQS6LAUOw4f1KOsRPn4CC3ALkkCgzFjgMEwSASokSaRTNDcrh2s5vs9S215cD3Ro/UyJIR2ROkgEITzdf16qvvt1T9vqLZn38Dn6NR3jkACcAD4AMIAVTyHubfyfw5+pSxHAADQAFIAcQA5nmP8++y/P8mf9591gTF5wTB82eLyVcB1Is+R9AauvrKxPlRZhBqY4WxFs45upgEgYiICO5iRZwWzsR1lg47cn5Y53oAYFLqsxIoDcB8/UcnzjkL5y5j+tc/W/lMIAULImcgBFAD0ASwkMLvnlFjfeKClXPtLZ+l1B0mtjWZp0GaZlxpRcZaR0QgIgghIDgnzpiTghuPi6QpxaDO/d4Cz44aIjtc8tK9Krc9AOcARgCmOaDsX/50SQNwX/vhkQPwCUDiM1jgAIKcgSaAKEawck7NmwNX2zpN+I3eTHd759Nmf3heGZ4PZZbEljGmODnNibRgZMAAAnEwJhwgiHPp+xVWq1ZXGqE/b4beqOnJ3iH3drte+vCanz1uSHsIYJgDmgFIAJhf/GDZfO2HR46ILoERvwSEzFmoA4gAdCZUu3HguneOU2/7YJSt7p70ooOjA18n88znblYXGNVqbBJKN6lIPvMFxR4nRUQwDlJZhKlx1dTZempm9WQaN09mrHrqhQv1au3aQijXTmWwcZry+7dqyXtt3+zmCznMTS4GgALML2OkAFGYUhNAJ0Zw/Zyatw5M8/WDqdv8cL+3trO34yFL5nUP++0G9dsVcbJUE712VQwaAR81AzateTwOJCkCkGgnZ5kNx5mrzTLbPE9ca5ja7kS5pbFOOkmso+PMW56ElVZsRDTTQXc9zN69FugHTc95pQASA8iughG/hIlFAN0pqi8dUeeNgzT40s5gvvHB473ofNhTDe4OVhb5/vWm2Hsp8vZWGvKoVRFDX9A0d1KVdwcAHidq+EwuX4zvZ8bVzlMbnc7d8tHMrg8yu36m1No8m7Z7CDdSKxtzQ4tnGXW2aupuK3AcACtFPvziB8tFVHsukIKJ7hTVl/fQ/crTuf/6hwfnG/cf3vOFycY3qtjbjLxHr3SDBxuR97QZ8F4p2iR5+NSl8Hk1+nkep6Bb4dVuBXvrDffkeG6vP53aW72UNkcuXZ8qG2Xwq5mTVWWt/wo0bwcAAJuPaUt/PwNSRKcg94nOFJWX9lz3K09m8svv7/VuPn78kDW56d2I+MNXO94Hr10L7rer4oAzGgIY5yDSEojiReXG8i5Kuaha8+jspuT9VuBO9qaud5jo0cDQ1sxR59zKl10mmRsZvOqsaYdk8gUqFkkBcOKKX1QBRHME1/dt642dWLz+n7u9mztPPqJI2sNbkbh3Zzl471YnuFfz2VHuhOMrMf/SSn33p9YZY/E3vyeotGgs/43IF2DCCJPFgCaBwKg2x3hvrpO+ptsTR8tjJ24yJa0Yp6lgLl3wqZwwLQAjSmw8c+6hq906SLwvfbh/trHz5CMWSXv4Slv815fXKnc3W/79QNAhgEEpzqsCwPd+BqeUhTEGWltYe9G/83eps9YBcA4g+/d/UKMrGT4FEIeCkus1pB5ziZxrfawJE9DK1MmNU21mx7HuL/gY5yasisUTORt+nuyikavceJrWXn98Ot/46PFHfkPY3q1I3HtjrXL3Zsv/MBC0XwqHSTHQ935GLss0lLroWhsYY2Gtg7UXFlbEfSLCt3+SOiIyb78ZFQyawq88Br0UkgU5uLkWZLicQXTn1tt4GrvXW77pd0Iq+6MRpSjVnDl/ZVcv3nk6MZv3d/cjH2r8UlM8vLMSvnfzgol9AKc5iBSA/v7PmVVKI00V0lR9AoRz7hmAMpDi81s/7jkimHfe6rqyE3scWArBtLOBjlVgHA9S5kUTYzcfTuwdydz5gk/FVkaJkm8sDEzl5sFcbD88Gq7NRmdqo872Xu16H2x1/Hu5OZWZMABsHKefAHFhTg4X8/44bxWJmMhdCpZEwDf/6tj+7HvXdOkHzGMQ10L4sTGNNMuaZwi358xfGxqzfZyovQUf/WI+LDereuxk90SFW4ejdHX/8MBb8N3pZiQevboU3K957Cj3iYKJwskwn6eYz1MkSYYsU8/YKJhwDs96GdDH/eI5Yyy+/qOjwsTS/F2DkONoJXD3I6YfBVCnjsibQa72U7Y1VejmUdZnxTakr8P1fkw39gejiDs1X62L/e2O/6BVFQc5E9OyTxSTKhi5DODyxD+rlUF97YeHNn9Hkr9z2PBwsOTbBzWofQE7T0lEI8NvHM3deg4kZPlZon6u5Epvqrr9056/4NPwekPs3Vj0nnK6FGL11dyglIExF+b0qwL4NEAlMDGAMSMM27572iC9J60eGgd/Znj3XNFKDqTCAFSmRrYGGVvuT2ZNsjrrhNS/sSj3Gj7rlUCo5yQ4GGOesfBFtXwsm78zBjCuSuq1PLsXwvRhbZY4ao4UWx5lrlUACY8zb2U4t92z8bQSCszaFX5yrS6PStuOtJzkLr/0iwVRtN/5i8MigqVF0lz0cFQhe8Ktm6UWlbFC93CGlcK0/GHKotO5bs3mU1n3aNStiV4UsmEJhH4eG+VI9GtqhYmlAGYNj4Y1bnvSmVGqrRxlrnWWuqhwdplqG06SNHDW2prHJu2KGHicpqWEYz/PufmLbvk23eZzSCTDtC7cQMBOnHE20S7IDEIAUgDg2lihVcYFh6pINqn5bPScDeCLagUrGYA0FBgJchOjrXLWce2cAMAFALLWwjpHHme66rNZw2fT8j7mRbBxpepSgFFVgakEZtYaDefI5psFlj9JzjknBNOhYHFFUnxld/miW5EoVSgQS04xEdPaOueso+J8AAIcZxySM+MJpgJO6ko96YUx8rt/fVqeg/MYlGBMEZhx1sHlc2MFMVIKCMbAGH16ae3/SGNMAEzAldb6ghFGRJyDOHELyNQ4WaprAS8Q2jtvtctzIOVIWmLSOeLOOjiLsmk5JxgRiAnjKEz0RUjLzyrsRYEotvv5HDgAmVgeGvDQgIS1jlx+NmAAHJwDgRwYE8pRdaZRy4GI/Bl6ESByIFQ658vY8lrmeDVTWlhrChyOATDkrCY4AyakclSPDZr59t4rgfmNN845rhYrUsubqXH1JEulMdYQnAZgGADFnI4FdELEWGqpPtXUUha1vKri/aZZuWCDFf7B8jkE2rHaxMjWNHP1JI4Zdy7hcDEAxQCkC9IMa1wPBCOVgDXHmrpTjSg/OfovghUhLrHhA6jOrIjGWnbP5mnTaKMaEoP2xZ4wZQDi6zUcNrju+QxzBVadW7Y0UrSc7/ULML8RVogIjF1iwy9kjLH1l0eKlk5Hs6pwdr7go7exKA4BxAzAfDGgwaJnj0KmRxbCmznRGSi2PtfoAmjkp0j562alMCkpvYKNojDSSJzoDnSwfjLTnbPJ2AuZHXUr7KgV8gGAOcuVoknku8MqMz0Ol84tRWPD14eKXbcOUQnMr83EPtZROH763UVWqrU1LCg6t5Xr/VSsHwzGkVM6XfCpt1QVh/mZaV5U/CbXG3wv8sxuQHqYWlYZWbHWU/zWVNNqLisUzv+FgymLQe+81Wal8m0NQBQ7f/VEV249Pc/WDk56lQq3w2tVtnszknuF3MCKikXDo95KhR7WSB3AmGxqefvMiM2TlG0nBssAWkXFopwoS0nrf2VOJRBFwbAOoJVBLPdtfXt/xjcfHZ22ndFZJ6CDm5H3sBmwonieinyfPwNwvlZnj/uxva91FqVKboylv35qMQqybHzNN6lkl84mKQDNGLNXK4m/StZm7MInSuZUgIg0+MoZGrf30/DVBydn6yenA7nk085Li/L+ZuQ9ziW6GYBMlA74oyhgh6+18F58aroDkzammkdD5m8JjURQlrQ9YyV7tmWYAEh+/v0lDcB+4y9P3OcBVAbAOS9Hp7IaEGnwtRHq2weqdufhabJ17/FOo0pmuNYQj756o/Jeu8oPc1nuWaWxKIhNAQyvVdnuRmLeTSbZ4iQV1RELO4z825SRBlK0PcMkQ1EzflbE/vn3lywA+60f991FachdAnQBgMAYQQiJn353kUoygyyJrS0DvjKi+va+ar5xb6Bu/8f9xx2tVLrU5Dt3lsN3rzflbqnWlha1X1eqIY0ABKtVenCWmI7Kkmoc08vn8Jed8GEzEtplQcfXfsBc8DxZ4Z//pHNJVvj2T84dEeHtN5tXZQV2RfJuAIgU5PIZNW4fqPqde6fZ7X//4KPlyXhi12q0s9323n+57T0A0C+xoQt9pBDwC18ZLgbMu72Iu2qo/OMMbOZw0wRyRctApo4Hsc0a13x1v8bNAfu4gPdcoeef/njhM4UeAA0HimIKVgdY2D5Iw1fv9WZbdz981JmMJ3algsevdPx3v3KjcrdTFU/zBZwVlfgCCEqsJLnt806F8d8COAYKx8raeRxuaGO6qfSC2Inm1KpuV2YPWlI9DS+08V9JeivJ3vUUXndMtet9U7v1dMY27x0M1t9/+KhhUpWuVrHzWjd495ubtX9bbcgnORtlSeMTGmLBTFzQ360wfAkw4sykx2k8m8Vy40yZaCbk9kR6rXMtVxa02WtLtdfk6qjKzVCQfa4Ymo8p8+4b8FoMP5oiXD6zlfV+KtZ3hsnavd3D9lGvJ0Oyw+Um23ml67//1Y3q3dWmfAKgVzKp8iI9FwgKPRuA7VSYEQxpNLP9J5P09TNlNieZWpsyvnomvMW6J1d7Ur5cF+6kIUyvJvQgYHYUMjP1ycSSrAIADS4ziDB1Xi0j0Zw5vzXWsnuW0dLRKO3sHJ9ET/YPKlbrrBtgZ70pHv32auXdWx3/Qacmnl7xCwXA/OE/ZO5vf997rs5e9pfiQGwWA5YtBhh3Qtv/74G6c5zo7VFKq+Mki85Aax6X3dDzN6oeH1WEP/EZTTyOmccoFpwUI4IlLrWjMHOsGivUx4mun07Om4f9YfV0OPSsMWmVu6NOgx283JL3v/5S7b31RW83B1C+MKAAmD/6R+3SVOVW+vybD2VmbEnnS5YqbOYxOt+fmr39id3qJ/rGSKEbJ2lzNJlVtXULzllL4EpyrrkQmjNhCIAFcWWsSJJUzOK5nMcJc9YqCTtf4O4oqrLeaoPvbnWCh7fa/uNOTTz3CgcA8+bbxqVphiTJcjf79LsorlTTKl9LShYDmi0Gor/RcE/2xma9P7cr/dgsD2LbPUtcK1Y2MNbyzIKMdQ4OKD7grLYXtSjTIJssVNmgXRG9azVxtNKQh1sdf28x5M+9VFM49ptva5ckGeI4Q5bpz3XNyZV07HLZMgYwqXt0/lpbHAGoDxPb2jnXK72ZiVJFobZM2IsqPV3UnRysw8UtGAcwctpjiJfqYrjV9g87VfGZ15zeegcuTTOkqcrVsQuZ71nC/f9y8ex/BgBWPkPRIWiRwQAAAABJRU5ErkJggg==";
        static InputControl input = new InputControl("input6");
        static SelectorControl selector = new SelectorControl("selector");
        /// <summary>
        /// Each plugin starts with Entry method inside static class Plugin
        /// </summary>
        /// <returns></returns>
        public static CategoryTab Entry()
        {
            List<Control> controls = new List<Control>();
            controls.Add(new InputControl("input1"));
            controls.Add(new InputControl("input2"));
            controls.Add(new InputControl("input3"));
            controls.Add(new InputControl("input5"));
            controls.Add(input);
            selector.Options = new List<String>() { "option1", "option2", "option3" };
            controls.Add(selector);

            // Each plugin represents a category containing a set of items or other categries
            // Each category works with a particular data type defined by enum TabType
            var category = new CategoryTab("Text", ContextType.Text, controls);

            #region Filling category with items
            var itemList = new List<ITab>();

            itemList.Add(new ItemTab("Trim ALL", ContextType.Text, new Func<ClipboardData, ClipboardData>(TrimAll), ccl_base64Image));
            itemList.Add(new ItemTab("Trim Start", ContextType.Text, new Func<ClipboardData, ClipboardData>(TrimStart), ccl_base64Image));
            itemList.Add(new ItemTab("Trim End", ContextType.Text, new Func<ClipboardData, ClipboardData>(TrimEnd), ccl_base64Image));
            itemList.Add(new ItemTab("To Upper", ContextType.Text, new Func<ClipboardData, ClipboardData>(ToUpper), ccl_base64Image));
            itemList.Add(new ItemTab("To Lower", ContextType.Text, new Func<ClipboardData, ClipboardData>(ToLower), ccl_base64Image));
            itemList.Add(new ItemTab("Extract URL", ContextType.Text, new Func<ClipboardData, ClipboardData>(ExtractURL), ccl_base64Image));
            itemList.Add(new ItemTab("Return Input", ContextType.Text, new Func<ClipboardData, ClipboardData>(ReturnInput), ccl_base64Image));

            category.Tabs = itemList;
            #endregion

            return category;
        }

        #region Trim
        static ClipboardData TrimAll(ClipboardData data)
        {
            StringBuilder trimmedText = new StringBuilder();
            try
            {
                var str = data.Data.ToString();
                char[] charArr = str.ToCharArray();

                for (int i = 1; i < charArr.Length; i++)
                {
                    if (charArr[i - 1] == ' ' && charArr[i] == ' ')
                    {
                        continue;
                    }
                    else
                    {
                        trimmedText.Append(charArr[i - 1]);
                    }
                }
                trimmedText.Append(charArr[charArr.Length - 1]);
            }
            catch
            {
                // do smth             
            }
            finally
            {
                // do smth 
            }
            return new ClipboardTextData(trimmedText, "");
        }

        static ClipboardData TrimStart(ClipboardData data)
        {
            return new ClipboardTextData(data.Data.ToString().TrimStart(), "");
        }

        static ClipboardData TrimEnd(ClipboardData data)
        {
            return new ClipboardTextData(data.Data.ToString().TrimEnd(), "");
        }
        #endregion

        #region 2Upper/Lower

        static ClipboardData ToUpper(ClipboardData data)
        {
            return new ClipboardTextData(data.Data.ToString().ToUpper(), "");
        }

        static ClipboardData ToLower(ClipboardData data)
        {
            return new ClipboardTextData(data.Data.ToString().ToLower(), "");
        }

        #endregion

        static ClipboardData ExtractURL(ClipboardData data)
        {
            Regex r = new Regex(@"https?://[a-zA-Z0-9_\-]+.[a-zA-Z0-9_\-]+(/[a-zA-Z0-9_\-]+)?(.[a-zA-Z0-9_\-]+)?");
            return new ClipboardTextData(r.Match(data.Data.ToString()).Value, "");
        }

        static ClipboardData ReturnInput(ClipboardData data)
        {
            return new ClipboardTextData(input.Value, "");
        }
    }
}
