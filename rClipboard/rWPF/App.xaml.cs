﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

using rLibrary;
using rCore;

namespace rWPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// Класс Application определяет ряд событий, который могут использоваться для всего приложения. 
    /// Методы событий описываются в App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Fields
        TabsHandler tabsHandler;
        #endregion

        #region Properties
        public Views.MainWindow mWindow
        {
            get;
            private set;
        }

        public DateTime CreationTime
        {
            get;
            set;
        }

        /// <summary>
        /// Tabs for categories/items access 
        /// </summary>
        public TabsHandler TabsHandler
        {
            get
            {
                return tabsHandler;
            }
            set
            {
                tabsHandler = value;
            }
        }

        /// <summary>
        /// User Settings
        /// </summary>

        #endregion

        public App()
        {
            // instantiating mWindow and Showing it
            mWindow = new Views.MainWindow();
            mWindow.Show();

            CreationTime = DateTime.Now;

            // searching for plugins
            string dllsPath = @"c:\Users\Rvach\Documents\Visual Studio 2013\Projects\rClipboard\rClipboard\rPlugin\bin\Debug";

            tabsHandler = new TabsHandler(LoadPlugins(dllsPath), new WindowsClipboard());

        }

        /// <summary>
        /// Plugin system
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        List<ITab> LoadPlugins(string path)
        {
            List<ITab> categories = new List<ITab>();
            foreach (string fileName in Directory.GetFiles(path, "*dll"))
            {
                var DLL = Assembly.LoadFile(fileName);
                if(fileName != "Library.dll")
                foreach (Type type in DLL.GetExportedTypes())
                {
                    // static method Entry of class Plugin is an entry method of every plugin 
                    if (type.Name == "Plugin")
                    {
                        System.Diagnostics.Debug.WriteLine("| " + type.Name);
                        System.Diagnostics.Debug.WriteLine("| " + type.GetMethod("Entry").Invoke(new Object(), new Object[] { }));

                        CategoryTab pluginCategory = (CategoryTab)type.GetMethod("Entry").Invoke(new Object(), new Object[] { });

                        categories.Add(pluginCategory);
                    }

                }
            }
            return categories;
        }

        /// <summary>
        /// Empty behaviour for NavigationTabs at early stage of development
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        Object DoSmth(Object obj)
        {
            MessageBox.Show("Some action here: " + obj);
            return obj;
        }

        #region EventHandlers
        private void App_Startup(object sender, StartupEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("| App starts");
        }

        #endregion
    }
}
