﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using rCore;

namespace rWPF
{
    class WindowsClipboard : AbstractClipboard
    {
        public override bool ContainsAudio()
        {
            return Clipboard.ContainsAudio();
        }

        public override bool ContainsImage()
        {
            return Clipboard.ContainsImage();
        }

        public override bool ContainsText()
        {
            return Clipboard.ContainsText();
        }

        public override object GetImage()
        {
            return Clipboard.GetImage();
        }

        public override object GetAudioStream()
        {
            return Clipboard.GetAudioStream();
        }

        public override string GetText()
        {
            return Clipboard.GetText();
        }

        public override bool SetText(string text)
        {
            Clipboard.SetText(text);
            return Clipboard.ContainsText();
        }

        public override object GetDataObject()
        {
            return Clipboard.GetDataObject();
        }
    }
}
