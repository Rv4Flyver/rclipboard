﻿using System;
using System.Collections.Generic;
using NHotkey.Wpf;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using rWPF;
using rCore;

namespace rWPF.Views
{
    /// <summary>
    /// Interaction logic for mWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Menu menu;
        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vlc);
        [DllImport("user32.dll")]
        public static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        ClipboardEdit clipboardEdit;

        public MainWindow()
        {
            InitializeComponent();
            System.Diagnostics.Debug.WriteLine("++++++++++++++++++++++++++++" + HotkeyManager.RegisterGlobalHotkeyProperty.Name);
            HotkeyManager.Current.AddOrReplace("mnmnmnmmn", Key.Space, ModifierKeys.Control, OnIncrementOrDecrement);

            //Task.Run(() => {
            //        while (true)
            //        {
            //            Thread.Set
            //            Thread.Sleep(1000); 
            //            UpdateListView(); 
            //        }
            //    });
            clipboardEdit = new ClipboardEdit(new WindowsClipboard());
        }

        private void UpdateBtn_Click(object sender, RoutedEventArgs e)
        {
            UpdateListView();
        }

        private void TrimBtn_Click(object sender, RoutedEventArgs e)
        {
            #region Trim
            if (TrimRadioAll.IsChecked.Value)
            {
                clipboardEdit.TrimAll();
                UpdateListView();
                return;
            }
            if (TrimRadioStart.IsChecked.Value)
            {
                clipboardEdit.TrimStart();
                UpdateListView();
                return;
            }
            if (TrimRadioEnd.IsChecked.Value)
            {
                clipboardEdit.TrimEnd();
                UpdateListView();
                return;
            }
            #endregion
        }

        private void ToLowerBtn_Click(object sender, RoutedEventArgs e)
        {
            clipboardEdit.ToLower();
            UpdateListView();
        }

        private void ToUpperBtn_Click(object sender, RoutedEventArgs e)
        {
            clipboardEdit.ToUpper();
            UpdateListView();
        }

        private void SurroundBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                try
                {
                    currentText = Clipboard.GetText();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
            }
            else
            {
                return;
            }

            if (SurroundTextTag.IsChecked.HasValue && SurroundTextTag.IsChecked.Value)
            {
                Clipboard.SetText("<" + SurroundText.Text + ">" + currentText + "</" + SurroundText.Text + ">");
            }
            else
            {
                Clipboard.SetText(SurroundText.Text + currentText + SurroundText.Text);
            }

            UpdateListView();
        }

        private void ReplaceWhitespacesBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                try
                {
                    currentText = Clipboard.GetText();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
            }
            else
            {
                return;
            }


            char replaceSymbol = ';';
            if (ReplaceWhitespacesWithCommas.IsChecked.HasValue && ReplaceWhitespacesWithCommas.IsChecked.Value)
            {
                replaceSymbol = ';';
            }
            if (ReplaceWhitespacesWithUnderline.IsChecked.HasValue && ReplaceWhitespacesWithUnderline.IsChecked.Value)
            {
                replaceSymbol = '_';
            }
            if (ReplaceWhitespacesWithPlusses.IsChecked.HasValue && ReplaceWhitespacesWithPlusses.IsChecked.Value)
            {
                replaceSymbol = '+';
            }
            if (ReplaceWhitespacesWithDots.IsChecked.HasValue && ReplaceWhitespacesWithDots.IsChecked.Value)
            {
                replaceSymbol = '.';
            }

            if (ReplaceWhitespacesInvert.IsChecked.HasValue && ReplaceWhitespacesInvert.IsChecked.Value)
            {
                Clipboard.SetText(currentText.Replace(replaceSymbol, ' '));
            }
            else
            {

                Clipboard.SetText(currentText.Replace(' ', replaceSymbol));
            }



            UpdateListView();
        }

        private void ExtractUrlBtn_Click(object sender, RoutedEventArgs e)
        {
            //


            if (Clipboard.ContainsText())
            {
                try
                {
                    currentText = Clipboard.GetText();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
            }
            else
            {
                return;
            }

            //Regex r = new Regex(@"http://\:\?[0-9]\?\(/*[a-zA-Z0-9_\-#]*\.*\)*?\?\(&*[a-zA-Z0-9;_+/.\-%]*-*=*[a-zA-Z0-9;_+/.\-%]*-*\)*");
            Regex r = new Regex(@"https?://[a-zA-Z0-9_\-]+.[a-zA-Z0-9_\-]+(/[a-zA-Z0-9_\-]+)?(.[a-zA-Z0-9_\-]+)?");
            currentText = r.Match(currentText).Value;

            System.Diagnostics.Debug.WriteLine(currentText);
            Clipboard.SetText(currentText);
            UpdateListView();


        }


        string currentText = string.Empty, previousText = string.Empty, shortText;

        void UpdateListView()
        {
            if (Clipboard.ContainsText())
            {
                try
                {
                    currentText = Clipboard.GetText();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
            }
            else
            {
                return;
            }

            if (!String.Equals(currentText, previousText))
            {
                if (currentText.Length > 40)
                {
                    shortText = currentText.Substring(0, 40);
                }
                else
                {
                    shortText = currentText;
                }

                ClipHistory.Items.Add(new
                {
                    Time = DateTime.Now.ToLongTimeString(),
                    Content = shortText,
                    ContentLength = currentText.Length
                });

                previousText = currentText;
            }
        }

        private void OnIncrementOrDecrement(object sender, NHotkey.HotkeyEventArgs e)
        {
            //this.Activate();
            //this.WindowState = WindowState.Normal;

            System.Drawing.Point point = System.Windows.Forms.Control.MousePosition;
            if (menu == null)
            {
                menu = new Menu();
                menu.Show();
                menu.Activate();
                menu.Left = point.X - 200;
                menu.Top = point.Y - 200;
            }
            else if (menu.IsActive)
            {
                menu.Hide();
            }
            else if (!menu.IsActive)
            {
                menu.Show();
                menu.Activate();
                menu.Left = point.X - 200;
                menu.Top = point.Y - 200;
            }
            else
            {
                menu = new Menu();
                menu.Show();
                menu.Activate();
                menu.Left = point.X - 200;
                menu.Top = point.Y - 200;
            }

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            System.Drawing.Point point = System.Windows.Forms.Control.MousePosition;
            Menu menu = new Menu();
            menu.Show();
            menu.Left = point.X - 200;
            menu.Top = point.Y - 200;
        }



    }
}
