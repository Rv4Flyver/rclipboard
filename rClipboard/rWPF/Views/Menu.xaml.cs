﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using rCore;
using rLibrary;

namespace rWPF.Views
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        App app;

        System.Drawing.Point mouseStartPoint;

        public System.Drawing.Point MouseStartPoint { get; set; }

        string top_base64Image = "iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAA9VSURBVHjazFprrF3XUf6+mVl773PPffn67diO4zzbOE0bcNPQFpKqlQBVgt+hKq1EhUqJoAiUFgmpVAiEEBQJIVr4U0RVqfCDIqEAElQKFYTIJSS10+bhV67t+HGv78P3cR57rRl+3GPXtdykdV4d6WgfHenstT6t+WbNfDPEazc+vM/YVKKX1otVRlWhFg8FkJtKegDQH3oHgKmwFI8yzFEmxzT3h16+ejIHgLiRxSM2/sbXsnkAVhy1CjpCjHtgQoiuBzpC1AAWABwf/Wc/gBkAAwA9D6wJsQJgtS3oJcUAQP7KiR8N1A0DeXifSb+N1CR2hZgCsAXAdgDbAGwlOU1iAkBXiLMeG0CE2O+BnUKsAVgBsARgDsAFAOc9MB+B5X7ra7Wx/cqJ7G8IkM89OM3Zl9etP/RuBDYDuEkFe0nuA7CHxE4VzJAcV0EDQEnOAnhi9IoHAOwlUQD0AaxGYAHAWQCnIuJkLjFbHGeEuFglWduzq5t/7z8W4nUD8vA+EwCNEJsA7AFwG4A7Sd6mgj0i3KaCSRHWKoQIBqZcInkIwJOj19wfEQcjMC3CGgBIDABcco8LxXGqlDhaPJ53j6MROJVLLBZH/++Ot/6agXzy7lqX10pXiB0A7gBwgOQBEreacqcKpk1Zm0kkY89MLpryZVU5LsJvRsTjEUBE/EwE3g9gP4ldJDcD6JCgewzcYymXONu2fsw9jrQ5juQSL+QS57odXfuLZ3rllYDYq5F60IaoMEVEDSCRdFMsqvBkMl4wE62SRJWkn5KspKQXVHlGTWYBHDPTOffAcJAPRcSCe+xVlZtEuI3CCRJNBOjFSymx3rZlsW3d2+ypbb3OJRIAAeCvFAReDUicWsw+05F+UsyJMAtxVoVjVZKkSqkrQVOr17W1qdK+qqylpCsUrg4Hee3cuZW+O7Bpuj6hJhcUOExyIiXtqkpDIgEQ94B7eNuWth2W9bYtyz3mxXa99M8tDP3VItl1XetDW0V3TmqTS2wSYrMHplXYkJDKGMnoVZK2qqStK22bxkpVm6ekECFFBSQREfDiaNuCUhxeNvYiSqSkYcmgKhIRSjK5e8ptSW1bZNDPHAyy9/qln7MvDVu/6IHFMwtt/5/PeXlVjoyIPQ5gjxAHANxD8mYSU6bUZOw3lVxMSV6uKz1bVbpc1dqraytqGmbKy+8txVFy2QCSHaVscFZNQlWiqoyioiLskJwCsLMU35XbvHk4LM1wkMtgUJYHw/JS2/rh3sCPuMepXGL1y0c3AsB1OfK5B6f5wsnVZkTsewE8JMJ3q+BmU3aTySAZT1eVHKmSnqgqvWgmc6rSc4+iEdEOM0nAPVCKI2dHyRun4j46keyoKg33YFWpkuyoaQ73mdGlebcId5tJHYE1AC+N7iptc7Skn/7jn9/ae/SxubguR144ucriUAhUhZnEBRU8m0xmk5FVkpWUZLau9LCZHDGT2dXV4crPPfyz+acf+YcbSjH+/U8+zG/84+M2Nd1MkDwK4BSAY+6xl+SEKiMlWSFxQYSZLFqceuzEJV7Nm2tdix+5xVJxTCXjFhVsTiZjyahNrVElGZjJkpnMj3WrhZxL77P/Ou+vQ76G339ok1S1dtphmRHhFhFOU1gjwIgopcR6KX5xvZfnS4nlP/3WWrvhWddw5OF9ZsUxkRRbSW5RwbgpOSJ2rmtt60rbutYQISgE4hoXKoFcHDH6/XvPQLkGrgpAEiIEiStPU4EqoSo0kyTCJEoDICV7DIdldTgs82vrea7NsfKXh3v5CpARwScB3C7ET5J8hyl2VEnQVLJUVXK6qe10VemiJWmBjYW9BPLI/9vWkcsGmCsg4ntkjGscj7z8JIS4AkaESCZISVJV6SZLultVdpOY3ggefq7fz9/u9fO3hkN/sT/0S198tu92FVc2CXEnyZ82xQOm3GHKVTP5bpV0zkwuifAsycHVZL58Grk4cg7ksnEClzce8YNSi8tAAj4CRgdMAdIBoBahirAW4W0A30ZyXFTOpaTT7rFSSswn4zqAoQFAvw2OVQySqySOq5BVkrEqyVJdyXNmcqhu7AiJuUcfm8t4E+yzD0wYgK05+zzJJQpPiHCaxLoqj5vJalNr9PqFV6LW9mnLK+tlAcARU55X5XgySlVJX1UWqkrPr68Nlz//n8sFb5L90RMr5Tff1VmYnKiecY9zdW3/5UBDwkVlNSXMRWDh5r2T+XKBJKbsusdWEW6vjOPJqHUlkZJ4XalvEHHDqcuIB22+7FIBd6D41e50Q5H4yhokMMqiYUpUlaJKEk1jbkmNBEv21VLifCk+t7LarhmAyj12kjyogoOjiwhmMp9MzorwoggHAGKUD70iiKs3dKMWsfFOgMgIsvVahRM5+05V2UxlrSanI/xQKThkxlkTwgBsIXGA5IOjW3xBlc+YyUkzORuBS+7h7oEyAlMc1wXxetlVYATApEd0IrA1In4igltF+JIoe1J4wlReNhGqe3QATKqgEeFAlaerpP9b1/YNNXnuvR/+wMr7P/W1wFtgX/7EXXz2mVMTQt4VEVUpsYuMySAakpMi7NS1qriHktwQEIRSJVmqkhxPSV4EcH5qU7f3VoEAgI/9zXMx1km9XPy8e7zoHsdH9b6IsKMmNQA1AJnEggq/Y8qsyp6ZfFtVnh+fqBfuffD9GV89ibfSbrljR37+yJmFNvvzIpwohZdEtUPyBTNdQCDzE3dVqc2xKRl31ZVMTXTToNOxC3Vj8zv3bF376Je+U/BjYJ//wCa9tDLs1pVuqWvdlpLWlnSZxMtj3XrRro40G5/vRZ2IwI+LDYcFOftVeRmhJlf2qm+f4LgI71Dlg3XiQ2Zyl5mMCdlrh+3qZ371g+3f/ttzbymIP/uFHTxzdq1bHPsJvJfEB83kPhFOkVzp99slG93uMwDeHoH3AXD3mIyIlbXVwdLTjz/ZB9C+lUBePLpovYHPJOOdpLxPNR5o2yIiNAS+ExEmIiweGEREzwOei0+7x/6c/fYIbF9a7HX+/tP38S3kBld73vHA9lzi9lxif84+XUp4zt5r2zIgWcw9iil7AC4B6JOsAex2j3cC0WvbsuWJx59b/uTdtfcGG7d5WwB/k/hz+MVLIuRUUtwu5N0AdgKoR2rlJZK9nL0YgAxgHsARITokdgMIEZ6PgIlwmyrHRRimhAegEZDY+P5G24jXYyIUU55X4SFV1qo8rcojZjIfEdkADEd1xhMAjqtw3EwEQCHRqkqbLKKkjdTEr6oz3qyoRpIqEBE+VSWmulKqympKer7ppLnBIA/tKyeyP3JvZ30w9HkAMio/x6vaVFXati0rVaXnh21/+a+eHQxvtI9xI/v/6K2paiqZArDdlDPJ2KQkbiarKcmcKOc3bZ1a/+W//u5GhXhhsbXJMZnxwIFc/KB73BoRY+6xpCrPhceh6an6yO/+lMz94X+vvCmF1a8daFQEM+44IIKDJO8yk2lTWVflMTU5JCJPzx47PwBQDADqxMgl6B7jpcT+UuI9JfsOJq6SvAXEdERsAXD6d97dHbStj7LfNybzHVntjt0ieJcKHxDh21Q5bibnVCVIftfd2XRSXKkQI5ABLLY5nk/ZJ9q2LIlwB0mIcgnAQISTqrKzeLQilyUlfl99/nql76PLOpGYVOFAhEdVOW9KqPKcCL8dEc+TXCzF8/fJQR+7LZkIJ8Ya2dppdEunsfG6NorSwyNHoB0OS5uzx7At1xUaXn+OI5FMyWgpiVRJo6p01Uzm3WNOTVYefexCvq5A98m761QlmZoYT1vq2jZbkjERagQit2XQtr7UH+R5FS70+qX3hafWXxeB7pF7O5KMncHQZ0hsATCtwpoEU5KSTNbr2i4CmK8bW/7Mv8xdX6ADgI/fnoRkd6yR3d0xu6ep7d6q0lssyQQAeomVnH12OCyHc/YjbfbZ5ZV25f737M2/8uWjN3QuX/rILfyfJ09bt6MTJPcCOCDEPaM23YSqhBlXRlrzM6o87B6nU6Vrjz4259cVse/YPxXHTl4qESg5h2X1bWZyt3vcTLLLUfVoJvsjYk9xHpscT3OHnz7Tf+TeTvlh9Kxr7Zmnz+pYow2ArRFxK8n7SNwjwt0irJPJmpm8ZCZrqjRRKWoo7/7QewOPff0H90c+fnuSKsl4lWTPWEcPNLXdk5LcrCpTFCqA9ZJ9IWc/27blwrD15Zx94B6lOCIiuHFZvjqvR+vrhhiHKZLbVLBThDMiHEsmxUyWU5KXzOQwySMUnoqI1cua8yv2EH9xl+q+7VUjwk2dxjabyXRVaaMmIsJwj1Kyl7YtuW09D9tS3OE5Oy+LET/MiYx6QbLRAYaJ0FSgZqKqZJXUzaRf1bokwot1nRZPnlzs//n/9V690XOtffTWpDu21M3EeDVRN7aJ5BSJsQikdpilbR1tW7zN3uYc/VJ8LWJjEGDY+trSaukDwGRXm2TsjppIEyS7AJqISCQFAEzpImzNuN7UtqzKxQisLC0P+l94av21NUN3b28EQFOKby3Zb1OT20nuAbBFTcYA6Ch09IWxkgUXSokzAGYb0WM3jdlLEUDOfjOAWz2wV4ibAGzbGCxgI0ICKCJcTybzqjwlwhdT0qNVbe2Om6aHeOrEa2uGkvReP7ej0Yu2ChUKN4lwH4CdJKdtQ8kIkeip8qJ7vFw8jkfgm+6xHhFQ5UH3eL8S+0nuIrGZZEcEJHm5L39WhMVMzphJC2KQc2mrOt1YM/Ra+/V3NFJX2nQa3aQqe8zkNlXeSfI2EHsQ2BYRk+5RRwCl+CACS+5xKGd/ctQzuT8CByNieqSww5QDVbkkwgsiPCXCoyJ8nsRRNTnlHosA+q/UTPqRRzi++Ev7eGZ20QbD0k0mm83kpqrSvaLcR3IPiZ0iMgNgPCIa91AvPtu2/sRIL34gF98rZBFh30xWRbiQkpwVlVMiPOkeswDOiPDi+ESz9rb778sPffrrr98Ix9X2G+/sSPFI05NVt65tyky2qMl2M9kmIltFZRrARLh3S/Gzw2E5Puro7i8ldgJYU+WKqiylSufM5IIlOy/CefdYXr3UW2s6qf2tfzr3xgzVXOuSv32wy6ZWE2E91q06VaXjqjJhybqj0YzaPRZKLsc3pJyy30vMRMRATXqqslbXtqKmq7ktvaq2QQTyp742++aMOV0P1B98cIabt07o0sKajU82WjeV5jZrBHJ3otPLbcbC/EqnFLeUtLhHyW0pu/ZuyWsrvfKjbv56QP5/AJK7fyooqjyDAAAAAElFTkSuQmCC";
        string lft_base64Image = "iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAooSURBVHjazJpdjF3XVcf/a629z7137p1Pz4xnWk/8UeeDxMEQktKoDSSlSG1UifKACqhIlZAoSoVEVHiAB5DgoQQkgkTTF0CCCiHBUyX6UIRoZEVVlVgNTWtSm8TjZOx4PB57PJ4P33vO3nv9eRiPYwVZtEaZmfVynq7W/p211n+vtc4VkgAAEcFdmvz6oSDtSm3tRglVEDMVK04DkNuV9v/uTJ3wPlu428N/7nAQACEVtFTQaZL3qiDDKuiS7KigBWClST4P4PKeA/nc4aB1ZhSRrghGO4ZJAPsBTAOYAjAGYNiJrgoWnZgH8Ld7BuRPf2FCzl/cDE3yrqrsM8UHg8k9InIIwByAWRFMAOgBaJMwAAsAHDtgPxLIbxyJevbt9XYwGY9B5lTlqKncbyZHTTGnKtMARki0AMCdtQhWReQigEt7AuR3jndss1+6wWQmmNwXgxxTlWMx6oeCyayqjKlKiwQB9EleJXERwLwIXhKRE3sBRABop6UxmLRi1BiDeox6LUZ7y0wuq6mJgCQGdK6783Ip/o6qLIjI2aoVlvcCCC+tND41GgdVpcuddsgx2mKsbChGi6qiqgIATiKV4oOUyqaarAPYKNk3L13aGOwayKdn1D44EdsqGK+i7gtBx1qVtWM0bbUDYzQP0RpVTSSTiJRS3N0dIgh0jAMYj5VhdrYHAN/dcZDPH40aTLok5qpKj8WoD7cqO1hVNhorsxB0YMGuqupFAIsArpfifS9ecna6U0rxm7erQoQ7D/Lc01Py5vz1dgw6E4Mcj1GfalX24aqygyFoV1VqABfofiq7nxPVqyWXZZL9pinFi7NpirhvdQuqjuiGHU+ts+fWRFWsimJVZbmKejkE/S8zWRARced6zr6Qs/8AwCmSC9dXB+sf/+Wfz5/4/W/wTk7+cAdA5D29lnzp0W40k9GhTpg0031mMiQiBgHprN256s4rsbKVpi79P37x2v954W372BGQLz7cCTHIcHcoTFWVTVaV9SyoAHAvzO5MOXsqxVkKkYuDBNx525Mo78EyBZ5/9cYrO5Jav/1QW9uV9qpKj4agj1aV/aQFnTFTkFgl/QKLXyjFr6XkKWWHO9+F4Ltv/b0vfweC8S5IDBLMZLxV2f0x2s+p6eMiMkNig+QPvfhyzr6Wki/m4vVWRLYisH3QOx14R0FKobR7xhB0w0zmRSAAhkrxVTpP5+wnAZwCsPwXr2zmH9fJCzsFcvCekdy/kVZitFNqugSg506lc1DXecWdS2vrzfW/+s9+wR41+dKjXR3uxa6ZTpnJfgvaI8GcSh4MsjbJpWkKciHcgeK3p9Pd5c32NCoCmApUgWCCEBQxKMwEZnrLhztRN0VTcq0bl5RZmswNdy6pynIu3AwhSAVgVlUes6CPAThAZ52zXy2Fiyn5ei6s3cHbIf6f4/Gt+ilOAIIM3hpdRBSqhKpAtpy0YtB9JGaLY7K4w5QXSDnpzpMAFkIwDaoyqSbHRORJdx4kuUziu06eB7DojrXi9PejcG+HKQKoE8UJdUJEoCoagowA6BXndDA5nhUTWeRtEfZJnFPBxdBqmalKR0RGSLbprEvhIslTKnIiBDl9/GcOrX/+b05zN3L/pRc+K9/+xreGzfw8CSuF+1Sla8p2LhgB0FEVUwBmQVuq0gGgAFbdOe/ON3LxpaFO7O8WBAA88cV/5uh4tw9gKUZ9o4o6X0VdVRU1RUdEWu60EKPlEGwFwOskcyneL8W/n7KfCaYrh++byTixuquKdPzJJ/LJb/7HysZ6fSYEHTbztWDSKUX+uzhXSGT56q/eE29s1uMkPpBTGU2p1HVdLtdNuTIyXG3+0beu7QnJ/doXHrTF88vdepAn+/08vb6ZWnXj11PmxRjkmt7e2OXsSMnRpIKcHU2zd66N25tbke3nu6ppT8xVPQD3leJP5uxPDer8QNP4UJPY7w/Kxq89Ppn+7czGrkK8+Pxn5NzrZ7upKUdK8Y+m5J8oxR9JmaPFud4krgYvHugykbM/mFL5WM70lDmSsq+nzNU33rw2AJB2E+R7J14Omxv1hKrc786PAXichN7sTF4HEFRESkqlztn7pdBz9rFceCQX3uvE/o2+d/7k4+OyWxD/8uwjsnqt3yGxP2e/151HcvExJ5xk34laVUrI2YuI9AGuARgAaAGYJfGQOzdSweQP3li7/itztiMbQxVBtK2WpdNSfOfEae20bVQEDwD4KQAHRKR186xrKujnwhLMJIvIFZKnzKRjJgdMpQ4ml51QI6eV0vOtBdz7DLHVf6kKgglUBWYiqjIkIkEES+58VQSiggs3O/IrAHJQ06bVCosAvlMK51uV9dzJXCSZspDiJLkTK4RtRQomiEFQxe0mUkUE5wC8GoJazu4ANkRkSRXLKGyEJP7ht37Clt652k1Nma7rMjWoc69pXFPmIBeuAFgaNH79a2dTA/z4kdmJmT0AwMLZpdAZqiYs8JhlfyyYfsgDhpy+qorT7jg50rVTzxyz5a+eGmTsQQsA0O5EuruISM9Mj4TAjxTnTHDZcMdhgGPumARw4Zlj7fou/OzM8qEUz6pyjeQZVRk2k9VgMuNbC8NVADXAERKzABK5VZR7akLczt/nnp4OJfuwqkzl7JNNU3pNKpKSe8rMJBOJdDc18pXv91/ZMZDtBd2ffWoq1oM8CmCyrvO+lH0oJTcSLM4awCqJK61KV1Jm/69f6++tBZ2I4LmnpzQ1pasqB0rhw01TjjepHM6Zw6W4kFgHsODErZXpZr+sf+RnD+Qv/OM57rpqbduHf/GjfOXfv11IFMBDCDoN4CEVP5gEXd9amV4AcYTkHICzQ21bfu17i4NnjrXLrhf7tj317Nf55U9ODkTkkoi8FqMWESwBOAhgNGU3ADcArhSXFslJEnFLDFBu1o/smvzebn/wzSv+uz/d2Tx0aPytuk7XReU0iTEAbScVAN1ZABQSGUAGUETgJGTXVetOq51nHxmysdFWWwTDpXB8UOfRnDnkzpgL9WYNuIgkAAOSmwDWAWykzM21zTL4p7dS2fGIvBf0gQf368Zav93UeYosR2PQe1U4l7JPimAIgLmDWxBYh8hlJ95RwYJVcrY3FN6+CbarIKxa0XPeSBDUIWja6rR9XFUOkZzNhWMkW+4gyT6Jq0JeBDCvKi+RvLETIPKj/Knmy5+cVABtVRkv2edIHHXn/e486s45d06X4iO5sHXzc0MtIqsiOCmCl//85Y2v7HZEbgnAi89/pv/Dl19NG+uDTXcuG7CgKmfceciLz6Xks9E5kbP33Nl20oLpB8xkZldU60721LNfJ4D0l780szbop35vpHNNVc6780xOeX+IPp2aMmUmY6VwGEDXTBbN9K09oVp3+t0Ln71HRBCaOrdCtE7JpVfXebgU75bsHRFpqclKCDb/e/966fJeBflfUN3hjl1cuBJCNFMVS6mYmeaJyeH+b/79m+/7FuZ/BgDf1TSqtmDTnQAAAABJRU5ErkJggg==";
        string rgt_base64Image = "iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAoNSURBVHjazJpbjJ3XVcf//7XW/r5zPFeP72PHTSakdRpXaY2cKEojJVV5qSp44gXRUoQKNKhS85QA4gHeEOIiIRAFIRVRVYWXVgi54lYpT21xE+LYTeP47vFlPPeZc86cy/fttXiYsZumqdsYesZLOjrfp3O29v593/6vvfZamxGBn7X9xgfK1Bt4E4CpMGePPKgjj+/Qujfw/JXLdQC4p4HcHj+HAfKpmbQXwAyAKQB9AF0PdIRoAWhXGd2k6AOov3zpvUHdHr9hCCbCXxRixgMHhOgAaAFYBbAAYN4UtzywGIG1X35AO6Wx+vKl2t9LH0MBaRTyIIAnARwmkQH0ALQjsAzgJoDZiLhc57japFwXYuk3Hy07D0yP1H/wX8tx34CMNHUOwI2IOBiBSRGWAECiD2DdPeazYzbnOJ89zrrHefeYvXCltfKpmdT7x4uV3xcgoyPpRER0IlABmCExTXIXgHESE+6x1z0O1TkerCp/yD3OVHWcEYm36hxzn3+82fnLU9287SDjE42FQb8+GRHL7nFYVQ6KcC+FYyQaEaBnzznHRlXllapyr2pPVeVlnSMBEAB+NycwFJC5ufbGzsnykprMK3Ca5FhKOqIqDRIJgLgH3MOrKlfVIG9UVV7rsl6pNnJvbnngP8mTDQXkwIHRD+fs8Lzl8wUg0ROVnqpIRKiRyd2TCJMIJwHsdI/D7uhNG1c/N+5LHli5vlz1/nXO87aAuMe050DOW5qlRF17iDgBqAibACZE5IAapiNilxfaiIgcgbU+cQXA6W7fz+yfsNnPjEb7S+d/2AEMBaQa5BtVleG++UakdhSFhnuwKFRJNtW0DveprUXzMREeMpMyAh0AVwDsA6BVHRXp1/74E3u6L55YiKGC/P5/LL3y4377zz/5JL/5tZdtYrIxRvI8gFkAF9zjMMkxVUZK0iIxL8KazJqdeuHSOt+um6GEKCR/4n/+8LmdUpTarAZ5SoS7RThJYYkAIyLnHBs5+9JGt17MOdb+9LudajNCGWKs9cKxHU/kdyxpKpuAIgSJO9+mAlVCVWgmm+JXGgDJtcdgkNuDQV7sbNQLVR2tvzrdrYc2teocB9/5vDbvYxOGuAOTcyCZAEBS5U6KHBKRQyQmAaAA5tzj9aKQ7wJ+7rcfa6z/zfd6PiSvhes/GrXennYB3wxXQAdMAdIBoBShirAU4c8BfJTkqKjMpaST7tHKORaTcQPAYCggf32m99/vtc3vPjVmAPbUtS+SXKXwkggnSWyo8qKZtBulRreXeV+J/d2afeEjzWJ8rJgQ4b6ytCkKGyQ8Au1c+8JgkOebO1LnxRPzmb/yoIkpR9xjjwj3FcbRZNSykEhJvCzUN4W4OZicHTkHqtpR1446B9yB7HFnutzrw7ndBwmoECKAKVEUiiJJNBrmltRIMNfezjlu5ewLrXbVMQCFexwgeVwFx7cWIpjJYjK5KcIlEfYBxFY8dFeI/8Mb+CH9ZA8ARI0gKy9VOFbXfkBVdlFZqsm1CD+ZM06a8aoJYQB2kzhK8lkVvM+Uy6o8ZSaXzeRmBNbdw90DeQsmO94V4v/L3gYjAMY9ohmBPRHx8xHcI8IrouxK5iVTuWEiVPdoAhhXQUOEfVVeK5K+Upb2TTV58+lPfqz1zO/8U2Ab7EufPcLvnZodE/JIRBQ5xzQZ40E0SI6LsFmWquIeSrJUQVOEUiRZLZJcTEnOAbg1sXOku10QAPCZv3szdjRTt85+yz3OucfFrf2+iLCpJiUANQA1iWUVvmHKWpVdM3ldVc6OjpXLjz/7TI2vXMZ22kPv31+fPXN9uar9rAjHcua6qDZJvmWmywjU/OyRIlV17EzG6bKQibGR1G82bb5s2OKBB/Z0Pv3FNzLuA/ujj+3U9dZgpCx0d1nq3pS0tKRrJG7sGClX7O2eZvPzA68zjDXmp7XBIKOu/W1xGaEmd8aqHxzjqAjfr8pny8TnzOSImewQslsNqvZLv/Xx6h/+7c1thfizX9rP6zc7I9kxQ+BpEh83k2MinCDZ6vWqVdsKHKcAfDACHwXg7jEeEa1Ou7/62svf6QGothPk3PkV6/Z9Khk/QMpHVeOpqsoiQkPgjYgwEWH2QD8iuh7wOvuke8zUtT8SgX2rK93mP79wjNuoDba73vTAvjrHI3WOmbr2yZzD69q7VZX7JLO5RzZlF8A6gB7JEsAh9/gwEN2qyru/9fKba597rPRuf3M1rzLgQ9LP6XPrIuREUjwi5GMADgAot7KV6yS7de3ZANQAFgGcEaJJ4hCAEOGtCJgI96pyVIRhSngAGgGJzeufed54U9c7RCimvKXCk6osVXlNlWfMZDEiagMwEOFNkt8CcFGFo2YiADKJSlWqZBE5bYYmHj8IIYbl1UhSBSLCV4vEVBZKVWmnpLcazbTQ79eD+zqM//TDqWgUMgFgnymnkrFRFOKN0tplqQup0Pl9B3d1fu1vv58N96k9f7ShIphyx1ERHCd5xEwmTWVDlRfU5KSIvHb1wq0+gOGAPH+08cQ9NCvdcUgEH1HhUyJ8VJWjZjKnKkHy++7ORjPF0JIPJA6+l/B9ayYmEuMq7IvwvCoXTQlVzonw9Yg4S3IlZx9eFgX40eTDXaDvXAK44YG3lDBViqqEqrRFuOg5Fki2X/rGgt9Xe/bPP96UZGz2Bz5FYjeASRWWJJiS5GSyUZa2BGCxbNjaS99YGH6C7m4gX/zVh/jt71yzkaaOkTwM4KgQH8JmmW5MVcKMrSLppaLQU6o87R7XUqGdF08s+FCLoXcT+6nXbuqOhjYA7ImIh0keI/EhER4SYZlMOmZyxUw6qjRRyWrIT/zC04ETXx9uNh54V7HHlg40AiWJia2d6rIIT4vwQjLJZrKWklwxk9Mkz2Az09h77oWvx9DLCj9O7CQiArIFY9xMhKgIVZU0EzeTXip0VYRLZZlWLl9e6f3F/3R9Wwo9K636lfERbSTjCIBRAGMkRwA0IiKRFAAwpYuwUmWrUdqaKlci0Jqf3+j9+asb218Mnd5T7qhrfx+Ahz1wWDbXlb0kxgA2REgAWYQbyWRRlbMiPJeSni9Kq/YfnBzg1UvbXwyNiD2qPO4ezygxQ3KaxC6STRGQZN+UqyRvijCbyXUzqUD06zpXRZnuj2KoKj8RgSdJHo+Iya0MO0y5rirrIpwX4awIz4vwLInzajLrHivu0fv1v3/r/jgw0GzY/pxjus6uQlkVYc9M2iJcTkluisqsCC+7x1UA10W4NDrW6Dz65LH6nd5pW0HK0i7n7GJZrgLoqLKlKqup0AUzmbdkt0S46B5r7fVupyhS9fxXrzq+evWn7mMoIEVp/1LXeUZzTEVEX026qtIpS2upabuuctfM+iTr3/v3pXs6uzUUkJ27RleWF1tvAG4pWXaPXFc57z2ws+60uvkLX7txzwfPbtv/DgADeC1ICA1u2gAAAABJRU5ErkJggg==";

        string ccl_base64Image = "iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAA/vSURBVHja3FrJb2THff5+tbylV/L1wuEyHGooDkdSDIxiHWzEWw4OcjOQS6LAUOw4f1KOsRPn4CC3ALkkCgzFjgMEwSASokSaRTNDcrh2s5vs9S215cD3Ro/UyJIR2ROkgEITzdf16qvvt1T9vqLZn38Dn6NR3jkACcAD4AMIAVTyHubfyfw5+pSxHAADQAFIAcQA5nmP8++y/P8mf9591gTF5wTB82eLyVcB1Is+R9AauvrKxPlRZhBqY4WxFs45upgEgYiICO5iRZwWzsR1lg47cn5Y53oAYFLqsxIoDcB8/UcnzjkL5y5j+tc/W/lMIAULImcgBFAD0ASwkMLvnlFjfeKClXPtLZ+l1B0mtjWZp0GaZlxpRcZaR0QgIgghIDgnzpiTghuPi6QpxaDO/d4Cz44aIjtc8tK9Krc9AOcARgCmOaDsX/50SQNwX/vhkQPwCUDiM1jgAIKcgSaAKEawck7NmwNX2zpN+I3eTHd759Nmf3heGZ4PZZbEljGmODnNibRgZMAAAnEwJhwgiHPp+xVWq1ZXGqE/b4beqOnJ3iH3drte+vCanz1uSHsIYJgDmgFIAJhf/GDZfO2HR46ILoERvwSEzFmoA4gAdCZUu3HguneOU2/7YJSt7p70ooOjA18n88znblYXGNVqbBJKN6lIPvMFxR4nRUQwDlJZhKlx1dTZempm9WQaN09mrHrqhQv1au3aQijXTmWwcZry+7dqyXtt3+zmCznMTS4GgALML2OkAFGYUhNAJ0Zw/Zyatw5M8/WDqdv8cL+3trO34yFL5nUP++0G9dsVcbJUE712VQwaAR81AzateTwOJCkCkGgnZ5kNx5mrzTLbPE9ca5ja7kS5pbFOOkmso+PMW56ElVZsRDTTQXc9zN69FugHTc95pQASA8iughG/hIlFAN0pqi8dUeeNgzT40s5gvvHB473ofNhTDe4OVhb5/vWm2Hsp8vZWGvKoVRFDX9A0d1KVdwcAHidq+EwuX4zvZ8bVzlMbnc7d8tHMrg8yu36m1No8m7Z7CDdSKxtzQ4tnGXW2aupuK3AcACtFPvziB8tFVHsukIKJ7hTVl/fQ/crTuf/6hwfnG/cf3vOFycY3qtjbjLxHr3SDBxuR97QZ8F4p2iR5+NSl8Hk1+nkep6Bb4dVuBXvrDffkeG6vP53aW72UNkcuXZ8qG2Xwq5mTVWWt/wo0bwcAAJuPaUt/PwNSRKcg94nOFJWX9lz3K09m8svv7/VuPn78kDW56d2I+MNXO94Hr10L7rer4oAzGgIY5yDSEojiReXG8i5Kuaha8+jspuT9VuBO9qaud5jo0cDQ1sxR59zKl10mmRsZvOqsaYdk8gUqFkkBcOKKX1QBRHME1/dt642dWLz+n7u9mztPPqJI2sNbkbh3Zzl471YnuFfz2VHuhOMrMf/SSn33p9YZY/E3vyeotGgs/43IF2DCCJPFgCaBwKg2x3hvrpO+ptsTR8tjJ24yJa0Yp6lgLl3wqZwwLQAjSmw8c+6hq906SLwvfbh/trHz5CMWSXv4Slv815fXKnc3W/79QNAhgEEpzqsCwPd+BqeUhTEGWltYe9G/83eps9YBcA4g+/d/UKMrGT4FEIeCkus1pB5ziZxrfawJE9DK1MmNU21mx7HuL/gY5yasisUTORt+nuyikavceJrWXn98Ot/46PFHfkPY3q1I3HtjrXL3Zsv/MBC0XwqHSTHQ935GLss0lLroWhsYY2Gtg7UXFlbEfSLCt3+SOiIyb78ZFQyawq88Br0UkgU5uLkWZLicQXTn1tt4GrvXW77pd0Iq+6MRpSjVnDl/ZVcv3nk6MZv3d/cjH2r8UlM8vLMSvnfzgol9AKc5iBSA/v7PmVVKI00V0lR9AoRz7hmAMpDi81s/7jkimHfe6rqyE3scWArBtLOBjlVgHA9S5kUTYzcfTuwdydz5gk/FVkaJkm8sDEzl5sFcbD88Gq7NRmdqo872Xu16H2x1/Hu5OZWZMABsHKefAHFhTg4X8/44bxWJmMhdCpZEwDf/6tj+7HvXdOkHzGMQ10L4sTGNNMuaZwi358xfGxqzfZyovQUf/WI+LDereuxk90SFW4ejdHX/8MBb8N3pZiQevboU3K957Cj3iYKJwskwn6eYz1MkSYYsU8/YKJhwDs96GdDH/eI5Yyy+/qOjwsTS/F2DkONoJXD3I6YfBVCnjsibQa72U7Y1VejmUdZnxTakr8P1fkw39gejiDs1X62L/e2O/6BVFQc5E9OyTxSTKhi5DODyxD+rlUF97YeHNn9Hkr9z2PBwsOTbBzWofQE7T0lEI8NvHM3deg4kZPlZon6u5Epvqrr9056/4NPwekPs3Vj0nnK6FGL11dyglIExF+b0qwL4NEAlMDGAMSMM27572iC9J60eGgd/Znj3XNFKDqTCAFSmRrYGGVvuT2ZNsjrrhNS/sSj3Gj7rlUCo5yQ4GGOesfBFtXwsm78zBjCuSuq1PLsXwvRhbZY4ao4UWx5lrlUACY8zb2U4t92z8bQSCszaFX5yrS6PStuOtJzkLr/0iwVRtN/5i8MigqVF0lz0cFQhe8Ktm6UWlbFC93CGlcK0/GHKotO5bs3mU1n3aNStiV4UsmEJhH4eG+VI9GtqhYmlAGYNj4Y1bnvSmVGqrRxlrnWWuqhwdplqG06SNHDW2prHJu2KGHicpqWEYz/PufmLbvk23eZzSCTDtC7cQMBOnHE20S7IDEIAUgDg2lihVcYFh6pINqn5bPScDeCLagUrGYA0FBgJchOjrXLWce2cAMAFALLWwjpHHme66rNZw2fT8j7mRbBxpepSgFFVgakEZtYaDefI5psFlj9JzjknBNOhYHFFUnxld/miW5EoVSgQS04xEdPaOueso+J8AAIcZxySM+MJpgJO6ko96YUx8rt/fVqeg/MYlGBMEZhx1sHlc2MFMVIKCMbAGH16ae3/SGNMAEzAldb6ghFGRJyDOHELyNQ4WaprAS8Q2jtvtctzIOVIWmLSOeLOOjiLsmk5JxgRiAnjKEz0RUjLzyrsRYEotvv5HDgAmVgeGvDQgIS1jlx+NmAAHJwDgRwYE8pRdaZRy4GI/Bl6ESByIFQ658vY8lrmeDVTWlhrChyOATDkrCY4AyakclSPDZr59t4rgfmNN845rhYrUsubqXH1JEulMdYQnAZgGADFnI4FdELEWGqpPtXUUha1vKri/aZZuWCDFf7B8jkE2rHaxMjWNHP1JI4Zdy7hcDEAxQCkC9IMa1wPBCOVgDXHmrpTjSg/OfovghUhLrHhA6jOrIjGWnbP5mnTaKMaEoP2xZ4wZQDi6zUcNrju+QxzBVadW7Y0UrSc7/ULML8RVogIjF1iwy9kjLH1l0eKlk5Hs6pwdr7go7exKA4BxAzAfDGgwaJnj0KmRxbCmznRGSi2PtfoAmjkp0j562alMCkpvYKNojDSSJzoDnSwfjLTnbPJ2AuZHXUr7KgV8gGAOcuVoknku8MqMz0Ol84tRWPD14eKXbcOUQnMr83EPtZROH763UVWqrU1LCg6t5Xr/VSsHwzGkVM6XfCpt1QVh/mZaV5U/CbXG3wv8sxuQHqYWlYZWbHWU/zWVNNqLisUzv+FgymLQe+81Wal8m0NQBQ7f/VEV249Pc/WDk56lQq3w2tVtnszknuF3MCKikXDo95KhR7WSB3AmGxqefvMiM2TlG0nBssAWkXFopwoS0nrf2VOJRBFwbAOoJVBLPdtfXt/xjcfHZ22ndFZJ6CDm5H3sBmwonieinyfPwNwvlZnj/uxva91FqVKboylv35qMQqybHzNN6lkl84mKQDNGLNXK4m/StZm7MInSuZUgIg0+MoZGrf30/DVBydn6yenA7nk085Li/L+ZuQ9ziW6GYBMlA74oyhgh6+18F58aroDkzammkdD5m8JjURQlrQ9YyV7tmWYAEh+/v0lDcB+4y9P3OcBVAbAOS9Hp7IaEGnwtRHq2weqdufhabJ17/FOo0pmuNYQj756o/Jeu8oPc1nuWaWxKIhNAQyvVdnuRmLeTSbZ4iQV1RELO4z825SRBlK0PcMkQ1EzflbE/vn3lywA+60f991FachdAnQBgMAYQQiJn353kUoygyyJrS0DvjKi+va+ar5xb6Bu/8f9xx2tVLrU5Dt3lsN3rzflbqnWlha1X1eqIY0ABKtVenCWmI7Kkmoc08vn8Jed8GEzEtplQcfXfsBc8DxZ4Z//pHNJVvj2T84dEeHtN5tXZQV2RfJuAIgU5PIZNW4fqPqde6fZ7X//4KPlyXhi12q0s9323n+57T0A0C+xoQt9pBDwC18ZLgbMu72Iu2qo/OMMbOZw0wRyRctApo4Hsc0a13x1v8bNAfu4gPdcoeef/njhM4UeAA0HimIKVgdY2D5Iw1fv9WZbdz981JmMJ3algsevdPx3v3KjcrdTFU/zBZwVlfgCCEqsJLnt806F8d8COAYKx8raeRxuaGO6qfSC2Inm1KpuV2YPWlI9DS+08V9JeivJ3vUUXndMtet9U7v1dMY27x0M1t9/+KhhUpWuVrHzWjd495ubtX9bbcgnORtlSeMTGmLBTFzQ360wfAkw4sykx2k8m8Vy40yZaCbk9kR6rXMtVxa02WtLtdfk6qjKzVCQfa4Ymo8p8+4b8FoMP5oiXD6zlfV+KtZ3hsnavd3D9lGvJ0Oyw+Um23ml67//1Y3q3dWmfAKgVzKp8iI9FwgKPRuA7VSYEQxpNLP9J5P09TNlNieZWpsyvnomvMW6J1d7Ur5cF+6kIUyvJvQgYHYUMjP1ycSSrAIADS4ziDB1Xi0j0Zw5vzXWsnuW0dLRKO3sHJ9ET/YPKlbrrBtgZ70pHv32auXdWx3/Qacmnl7xCwXA/OE/ZO5vf997rs5e9pfiQGwWA5YtBhh3Qtv/74G6c5zo7VFKq+Mki85Aax6X3dDzN6oeH1WEP/EZTTyOmccoFpwUI4IlLrWjMHOsGivUx4mun07Om4f9YfV0OPSsMWmVu6NOgx283JL3v/5S7b31RW83B1C+MKAAmD/6R+3SVOVW+vybD2VmbEnnS5YqbOYxOt+fmr39id3qJ/rGSKEbJ2lzNJlVtXULzllL4EpyrrkQmjNhCIAFcWWsSJJUzOK5nMcJc9YqCTtf4O4oqrLeaoPvbnWCh7fa/uNOTTz3CgcA8+bbxqVphiTJcjf79LsorlTTKl9LShYDmi0Gor/RcE/2xma9P7cr/dgsD2LbPUtcK1Y2MNbyzIKMdQ4OKD7grLYXtSjTIJssVNmgXRG9azVxtNKQh1sdf28x5M+9VFM49ptva5ckGeI4Q5bpz3XNyZV07HLZMgYwqXt0/lpbHAGoDxPb2jnXK72ZiVJFobZM2IsqPV3UnRysw8UtGAcwctpjiJfqYrjV9g87VfGZ15zeegcuTTOkqcrVsQuZ71nC/f9y8ex/BgBWPkPRIWiRwQAAAABJRU5ErkJggg==";


        TabsHandler tabsHandler;

        public Menu()
        {
            InitializeComponent();
            app = (App)Application.Current;

            tabsHandler = app.TabsHandler;

            #region Control Tabs Initialization
            Segment_top_textBox.Text = "Go home";
            Segment_left_textBox.Text = "Backward";
            Segment_right_textBox.Text = "Forward";

            Segment_top_img.Source = BitmapFromBase64(top_base64Image);
            Segment_left_img.Source = BitmapFromBase64(lft_base64Image);
            Segment_right_img.Source = BitmapFromBase64(rgt_base64Image);

            Segment_top_img.Opacity = 0.5;
            Segment_left_img.Opacity = 0.5;
            Segment_right_img.Opacity = 0.5;
            #endregion

            UpdateView();

            #region Temporary - Stackpanel created here for all plugins
            StackPanel stackPanel = new StackPanel();
            stackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;

            Expander expander = new Expander();

            expander.Header = "Options";
            expander.Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. \nSed tempus leo ac risus gravida, iaculis convallis quam tempor. \nAliquam id facilisis augue. \nSed congue eget sapien in congue. In ac ornare lorem, vitae pulvinar ex. Proin pretium ultricies arcu, at consectetur quam placerat vel. \nMaecenas volutpat, magna sit amet placerat dignissim, odio massa pretium mauris, eu sollicitudin nulla dui vitae magna. \nNunc eu leo sem. Cras interdum ipsum sit amet ipsum congue, in ornare justo gravida. \nCras fringilla nunc enim. Proin at hendrerit magna. \nEtiam commodo nulla nibh, vel tristique felis condimentum eget. \nDonec accumsan porta justo nec cursus. \nNullam vitae libero lacus. \nMorbi bibendum consequat augue eget porttitor. Fusce metus nisl, luctus non dignissim accumsan, maximus quis turpis. Praesent in aliquet purus.\nQuisque justo ligula, malesuada nec arcu venenatis, suscipit eleifend lectus. Proin nec sagittis enim. Vestibulum sit amet hendrerit ipsum. Morbi sed diam magna. Ut dignissim dignissim urna. Suspendisse eu varius quam, nec placerat neque. Integer aliquam venenatis nulla id efficitur. Quisque eget velit tortor. In ultricies nec nunc nec interdum. Duis efficitur vel sapien a pellentesque. Aliquam neque nulla, accumsan sed velit ut, fermentum posuere justo. Nullam posuere ex ac molestie mattis. Morbi nec molestie justo.";
            expander.Width = 140;
            expander.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            stackPanel.Children.Add(expander);

            if (stackPanel != null)
            {
                Grid_Options_ScrollViewer.Content = stackPanel;
                Grid_Options_CloseBtn.Visibility = System.Windows.Visibility.Visible;
            }

            #endregion

            for (int i = 0; i <= 5; i++)
            {
                string name = "Segment" + i;
                Ellipse segment = (Ellipse)this.FindName(name);
                segment.MouseEnter += TabAction;
            }

            mouseStartPoint = System.Windows.Forms.Control.MousePosition;
        }

        void UpdateView()
        {
            for (int i = 0; i <= 5; i++)
            {
                TextBox text = (TextBox)this.FindName("Segment" + i + "_textBox");
                text.Text = tabsHandler.TabsSet[i].Label;

                Image image = (Image)this.FindName("Segment" + i + "_img");
                image.Source = tabsHandler.TabsSet[i].Base64Image != String.Empty ? BitmapFromBase64(tabsHandler.TabsSet[i].Base64Image) : null;
                image.Opacity = 0.25;
            }
        }

        private void TabAction(object sender, MouseEventArgs e)
        {
            int i;
            string tabName = sender.GetType().GetProperty("Name").GetValue(sender).ToString();
            string digit = System.Text.RegularExpressions.Regex.Match(tabName, @"\d").Value;
            if (!Int32.TryParse(digit, out i)) return;

            if (tabsHandler.Action(i))
            {
                this.Hide();
            }
            else
            {
                //StackPanel stackPanel = (StackPanel)Grid_Options_ScrollViewer.Content;
                //Expander expander = (Expander)stackPanel.Children[0];
                Expander expander = new Expander();
                WrapPanel panel = new WrapPanel();
                Grid_Options_ScrollViewer.Content = expander;

                foreach (var control in tabsHandler.Controls)
                {
                    if (control.GetType() == typeof(rLibrary.Controls.InputControl))
                    {
                        var input = new TextBox();
                        input.Name = control.Name;
                        input.Width = control.Width;
                        input.Height = control.Height;
                        input.TextChanged += inputControlChanged;

                        var label = new Label();
                        label.Content = control.Name;

                        panel.Children.Add(label);
                        panel.Children.Add(input);
                    }

                    if (control.GetType() == typeof(rLibrary.Controls.SelectorControl))
                    {
                        if (((rLibrary.Controls.SelectorControl)control).Options.Count > 2)
                        {
                            foreach (var option in ((rLibrary.Controls.SelectorControl)control).Options)
                            {
                                var radioButton = new RadioButton();
                                radioButton.GroupName = control.Name;
                                radioButton.Content = option;
                                panel.Children.Add(radioButton);
                            }
                        }
                    }
                }
                expander.Content = panel;

                expander.Header = "Category Options";
                expander.Width = 140;

                //Grid_Options_ScrollViewer.Content = stackPanel;
            }

            UpdateView();

            System.Windows.Forms.Cursor.Position = mouseStartPoint;
        }

        // TextChanged
        void inputControlChanged(object sender, TextChangedEventArgs e)
        {
            String cName = ((TextBox)sender).Name;
            String cText = ((TextBox)sender).Text;
            tabsHandler.Controls.Find(c => c.Name == cName).Value = cText;
        }


        void CloseBtn_Click(object sender, RoutedEventArgs args)
        {
            this.Hide();
        }

        public static BitmapSource BitmapFromBase64(string b64string)
        {
            var bytes = Convert.FromBase64String(b64string);

            using (var stream = new MemoryStream(bytes))
            {
                return BitmapFrame.Create(stream,
                    BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
            }
        }

        #region General Controls
        // TOP control
        private void Segment_top_MouseEnter(object sender, MouseEventArgs e)
        {
            tabsHandler.GoHome();
            UpdateView();
            System.Windows.Forms.Cursor.Position = mouseStartPoint;
        }
        private void Segment_top_MouseLeave(object sender, MouseEventArgs e)
        {
            Segment_top_BlurEffect.Radius = 5;
        }
        private void Segment_top_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        // LEFT Control
        private void Segment_left_MouseEnter(object sender, MouseEventArgs e)
        {
            Segment_left_BlurEffect.Radius = 2;
        }
        private void Segment_left_MouseLeave(object sender, MouseEventArgs e)
        {
            Segment_left_BlurEffect.Radius = 5;
        }
        private void Segment_left_MouseDown(object sender, MouseButtonEventArgs e)
        {
            tabsHandler.Backward();
            UpdateView();
        }

        // RIGHT Control
        private void Segment_right_MouseEnter(object sender, MouseEventArgs e)
        {
            Segment_right_BlurEffect.Radius = 2;
        }
        private void Segment_right_MouseLeave(object sender, MouseEventArgs e)
        {
            Segment_right_BlurEffect.Radius = 5;
        }
        private void Segment_right_MouseDown(object sender, MouseButtonEventArgs e)
        {
            tabsHandler.Forward();
            UpdateView();
        }
        #endregion

        private void Grid_Options_CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Grid_Options_ScrollViewer.Visibility == System.Windows.Visibility.Collapsed)
            {
                Grid_Options_ScrollViewer.Visibility = System.Windows.Visibility.Visible;
                Grid_Options_CloseBtn.Content = "hide";
            }
            else
            {
                Grid_Options_ScrollViewer.Visibility = System.Windows.Visibility.Collapsed;
                Grid_Options_CloseBtn.Content = "show";
            }
        }






    }


}
