﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rCore
{
    public interface IRepository<T> where T : class
    {
        T Get(int id);
        T Find(T entity);
        void Add(T entity);
        void Delete(int id);
    }
}
