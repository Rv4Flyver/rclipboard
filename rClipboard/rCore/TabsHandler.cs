﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using rLibrary;
using rLibrary.Models;
using rLibrary.Controls;

namespace rCore
{
    public class TabsHandler
    {
        List<ITab> plugins;            // default tabs represented by root categories of loaded plugins
        List<ITab> tabs;               // current tabs
        List<ITab> tabsSet;            // current tabs that are displayed
        List<Control> controls;

        Stack<List<ITab>> backwardTabs;       // previous tabs set or category or home
        Stack<List<ITab>> forwardTabs;        // next tabs set or category or home

        ContextType currentContext;     // current context (the way the system determines it depends on settings)
        int currentTabSet;      // number of current tab set 

        ClipboardHandler clipboard;

        public TabsHandler(List<ITab> plugins, AbstractClipboard clipboard)
        {
            this.plugins = tabs = plugins;
            if (tabs.Count < 6)
            {
                int stubsCount = 6 - tabs.Count;
                for (int i = 0; i < stubsCount; i++)
                {
                    tabs.Add(new ItemTab("...", ContextType.Undefined, (data) => { return data; }));
                }
            }
            tabsSet = tabs.Take(6).ToList<ITab>();
            backwardTabs = new Stack<List<ITab>>();
            forwardTabs = new Stack<List<ITab>>();
            currentContext = ContextType.Undefined;
            currentTabSet = 0;

            this.clipboard = new ClipboardHandler(clipboard);
        }

        #region Data
        public void Add(ClipboardData tab)
        {
            throw new NotImplementedException();
        }

        private List<ITab> Tabs
        {
            get
            {
                return tabs;
            }
            set
            {
                tabs = value;
                tabsSet = tabs.Take(6).ToList<ITab>();  // updates tabsSet every time some changes was made in it
            }
        }

        public List<ITab> TabsSet
        {
            get { return tabsSet; }
        }

        public List<Control> Controls
        {
            get { return controls; }
        }
        #endregion

        #region Actions
        public void GoHome()
        {
            currentContext = ContextType.Undefined;
            currentTabSet = 0;
            tabs = plugins;

            tabsSet = tabs.Take(6).ToList<ITab>();

            forwardTabs.Clear();            // all forward history lost
        }

        public bool Action(int tabsSetID)
        {
            if (tabsSet[tabsSetID].GetType() == typeof(CategoryTab))
            {
                backwardTabs.Push(tabs);                             // pushing to the history current tabs

                currentContext = tabsSet[tabsSetID].TabType;            // getting context Type  ??? what is 

                tabs = ((CategoryTab)tabsSet[tabsSetID]).Expand();

                controls = ((CategoryTab)tabsSet[tabsSetID]).Controls;

                if (tabs.Count > 6)
                {
                    tabsSet = tabs.Take(6).ToList<ITab>();
                }
                else
                {
                    tabsSet = tabs.Take(tabs.Count).ToList<ITab>();
                    for (int i = 0; i < 6 - tabs.Count; i++)
                    {
                        tabsSet.Add(new ItemTab("...", ContextType.Undefined, (data) => { return data; }));
                    }
                }
                currentTabSet = 0;

                return false;
            }
            else
            {
                clipboard.SetData(((ItemTab)tabsSet[tabsSetID]).Execute(clipboard.GetData()));
                //controls = null;
                return true;
            }
        }
        #endregion

        #region History Actions
        public List<ITab> Forward()
        {
            if (TakeNextSet())              // check if there is another set of tabs in tabs list
            {
                return tabsSet;
            }

            if (forwardTabs.Count > 0)      // check if there is there any elements in the history
            {
                backwardTabs.Push(tabs);
                return Tabs = forwardTabs.Pop();
            }

            return null;                    // if two former checks failed - return null
        }

        public List<ITab> Backward()
        {
            if (TakePrevSet())              // check if there is another set of tabs in tabs list
            {
                return tabsSet;
            }

            if (backwardTabs.Count > 0)     // check if there is there any elements in the history
            {
                forwardTabs.Push(tabs);
                return Tabs = backwardTabs.Pop();
            }

            return null;                    // if two former checks failed - return null
        }

        bool TakeNextSet()
        {
            if (tabs.Count > 6 && (tabs.Count - (currentTabSet + 1) * 6) > 0)
            {
                currentTabSet++;
                int tabsLeft = tabs.Count - currentTabSet * 6;
                if (tabsLeft < 6)
                {
                    // taking the rest of tabs and filling empty tabSet elements with dummies
                    //currentTabSet++;
                    tabsSet = tabs.Skip(currentTabSet * 6).Take(tabsLeft).ToList<ITab>();

                    for (int i = 0; i < 6 - tabsLeft; i++)
                    {
                        tabsSet.Add(new ItemTab("...", ContextType.Undefined, (data) => { return data; }));
                    }
                }
                else
                {
                    //currentTabSet++;
                    tabsSet = tabs.Skip(currentTabSet * 6).Take(6).ToList<ITab>();
                }
                return true;
            }
            currentTabSet = 0;
            return false;
        }

        bool TakePrevSet()
        {
            if (currentTabSet * 6 > 0)
            {
                currentTabSet--;
                tabsSet = tabs.Skip(currentTabSet * 6).Take(6).ToList<ITab>();
                return true;
            }
            return false;
        }
        #endregion

    }
}
