﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;

namespace rCore
{
    public class ClipboardEdit
    {
        static string currentText = string.Empty, previousText = string.Empty, shortText;

        AbstractClipboard clipboard;

        public ClipboardEdit(AbstractClipboard clipboard)
        {
            this.clipboard = clipboard;
        }

        public bool IsContainsText()
        {
            if (clipboard.ContainsText())
            {
                try
                {
                    currentText = clipboard.GetText();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        #region Trims
        public void TrimAll()
        {
            if (IsContainsText())
            {
                currentText = currentText.Trim();

                char[] charArr = currentText.ToCharArray();
                StringBuilder trimmedText = new StringBuilder();
                for (int i = 1; i < charArr.Length; i++)
                {
                    if (charArr[i - 1] == ' ' && charArr[i] == ' ')
                    {
                        continue;
                    }
                    else
                    {
                        trimmedText.Append(charArr[i - 1]);
                    }
                }
                trimmedText.Append(charArr[charArr.Length - 1]);

                currentText = trimmedText.ToString();

                clipboard.SetText(currentText.Trim());
            }

        }

        public void TrimStart()
        {
            if (IsContainsText())
            {
                clipboard.SetText(currentText.TrimStart());
            }
        }

        public void TrimEnd()
        {
            if (IsContainsText())
            {
                clipboard.SetText(currentText.TrimEnd());
            }
        }
        #endregion

        #region 2Upper/Lower
        public void ToUpper()
        {
            if (IsContainsText())
            {
                clipboard.SetText(currentText.ToUpper());
            }
        }

        public void ToLower()
        {
            if (IsContainsText())
            {
                clipboard.SetText(currentText.ToLower());
            }
        }
        #endregion


    }
}
