﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

using rLibrary;
using rLibrary.Models;

namespace rCore
{
    class ClipboardHandler
    {
        private AbstractClipboard clipboard;

        public ClipboardHandler(AbstractClipboard clipboard)
        {
            this.clipboard = clipboard;
        }

        public ClipboardData Data
        {
            get;
            private set;
        }

        public ContextType ContextType
        {
            get;
            private set;
        }

        public ClipboardData GetData()
        {
            if (clipboard.ContainsAudio())
            {
                ContextType = ContextType.Audio;
                return Data = new ClipboardAudioData(clipboard.GetAudioStream(), "audiofile");
            }

            if (clipboard.ContainsImage())
            {
                ContextType = ContextType.Image;
                return Data = new ClipboardImageData(clipboard.GetImage(), "image");
            }

            if (clipboard.ContainsText())
            {
                ContextType = ContextType.Text;
                return Data = new ClipboardImageData(clipboard.GetText(), "text");
            }

            throw new NotImplementedException("ClipboardHandler have no implementation for this type of data " + clipboard.GetDataObject());
        }

        public void SetData(ClipboardData data)
        {
            if (data.GetType() == typeof(ClipboardTextData))
            {
                clipboard.SetText(data.Data.ToString());
                return;
            }

            System.Diagnostics.Debug.WriteLine("| ERROR: Set clause of this type of data has not implemented yet :(");
        }

    }
}
