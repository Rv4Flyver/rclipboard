﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rCore
{
    public abstract class AbstractClipboard
    {
        public abstract bool ContainsAudio();

        public abstract bool ContainsImage();

        public abstract bool ContainsText();

        public abstract Object GetImage();

        public abstract Object GetAudioStream();

        public abstract String GetText();

        public abstract bool SetText(String text);

        public abstract Object GetDataObject();
    }
}
